const express = require('express')
const bodyParser = require('body-parser');
const appServer = express()
const app = express()
const port = 5050;

app.use(bodyParser.json())
appServer.use(bodyParser.json())
const CheckJson =   bodyParser.json()

app.get('/belajarweb', (req, res) => {
    res.send('Belajar Web 3');
});

app.post('/methodpost', CheckJson, (req, res) => {
    res.json({  
      Nama : 'Megasuria Widya Tantri',
      Kelas : '3IFP-01',
    });
  });
  
app.post('/tampil',CheckJson,function(req,res){
    var nama = req.body.nama;
    var kelas = req.body.kelas;
    res.send('nama : ' + nama + ',' + kelas);
})

// app.post('/tampil', CheckJson, (req, res) => {
//     res.send({
//       body: req.body.nama
//     });
//   });

  app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
  })